package hr.markic.ivanmarkicdz2.model.vrijeme;

import org.junit.jupiter.api.Test;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RasporedTest {

    @Test
    void dodajTermin_TerminIsNull_ReturnFalse() {
        //Arange
        var raspored = new Raspored();

        //Act
        boolean success = raspored.dodajTermin(null);

        //Assert
        assertFalse(success);
    }

    @Test
    void dodajTermin_TerminIsNotNull_ReturnTrue() {
        //Arange
        var raspored = new Raspored();
        var termin = new Termin();
        termin.setVrijemePocetka(new Date());

        //Act
        boolean success = raspored.dodajTermin(termin);

        //Assert
        assertTrue(success);
    }

    @Test
    void obrisiTermin_TerminIsNull_ReturnFalse() {
        //Arange
        var raspored = new Raspored();
        raspored.getTermini().add(null);
        //Act
        boolean success = raspored.obrisiTermin(null);

        //Assert
        assertFalse(success);
    }

    @Test
    void obrisiTermin_TerminNotIsNull_ReturnTrue() {
        //Arange
        var raspored = new Raspored();
        var termin = new Termin();
        termin.setVrijemePocetka(new Date());
        raspored.getTermini().add(termin);

        //Act
        boolean success = raspored.obrisiTermin(termin);

        //Assert
        assertTrue(success);
    }
}