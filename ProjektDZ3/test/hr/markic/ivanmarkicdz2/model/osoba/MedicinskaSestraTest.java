package hr.markic.ivanmarkicdz2.model.osoba;

import hr.markic.ivanmarkicdz2.model.narudzba.Racun;
import hr.markic.ivanmarkicdz2.model.papir.Uputnica;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MedicinskaSestraTest {

    @Test
    void izdajRacun_ReturnTypeRacun() {
        //Arange
        var medicinskaSestra = new MedicinskaSestra();

        //Act

        var racun = medicinskaSestra.izdajRacun();

        //Assert
        assertEquals(Racun.class.getName(),racun.getClass().getName());
    }

    @Test
    void izdajUputnicu_ReturnTypeUputnica() {
        //Arange
        var medicinskaSestra = new MedicinskaSestra();

        //Act

        var uputnica = medicinskaSestra.izdajUputnicu();

        assertEquals(Uputnica.class.getName(),uputnica.getClass().getName());
    }
}