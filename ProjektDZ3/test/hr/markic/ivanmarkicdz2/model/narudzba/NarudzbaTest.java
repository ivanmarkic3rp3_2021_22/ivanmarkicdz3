package hr.markic.ivanmarkicdz2.model.narudzba;

import hr.markic.ivanmarkicdz2.model.osoba.ZaposlenikSluzbeNabave;
import hr.markic.ivanmarkicdz2.model.vrijeme.Raspored;
import hr.markic.ivanmarkicdz2.model.vrijeme.Termin;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class NarudzbaTest {

    @Test
    void dodajStavku_StavkaIsNull_ReturnFalse() {

        //Arange
        var narudzba = new Narudzba(new ZaposlenikSluzbeNabave());

        //Act
        boolean success = narudzba.dodajStavku(null);

        //Assert
        assertFalse(success);
    }

    @Test
    void dodajStavku_StavkaIsNotNull_ReturnTrue() {

        //Arange
        var narudzba = new Narudzba(new ZaposlenikSluzbeNabave());
        var stavkaNarudzbe = new StavkaNarudzbe();


        //Act
        boolean success = narudzba.dodajStavku(stavkaNarudzbe);

        //Assert
        assertTrue(success);
    }

    @Test
    void obrisiStavku_StavkaIsNotNull_ReturnTrue() {

        //Arange
        var narudzba = new Narudzba(new ZaposlenikSluzbeNabave());
        var stavkaNarudzbe = new StavkaNarudzbe();
        narudzba.getStavkeNarudzbe().add(stavkaNarudzbe);

        //Act
        boolean success = narudzba.obrisiStavku(stavkaNarudzbe);

        //Assert
        assertTrue(success);
    }

    @Test
    void obrisiStavku_StavkaIsNull_ReturnFalse() {

        //Arange
        var narudzba = new Narudzba(new ZaposlenikSluzbeNabave());
        narudzba.getStavkeNarudzbe().add(null);

        //Act
        boolean success = narudzba.obrisiStavku(null);

        //Assert
        assertFalse(success);
    }
}