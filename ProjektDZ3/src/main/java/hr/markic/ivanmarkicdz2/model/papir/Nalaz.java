package hr.markic.ivanmarkicdz2.model.papir;

import hr.markic.ivanmarkicdz2.model.osoba.Lijecnik;
import hr.markic.ivanmarkicdz2.model.osoba.Pacijent;

public class Nalaz {

    private String nalaz;
    private Lijecnik lijecnik;

    public Pacijent getPacijent() {
        return pacijent;
    }

    public void setPacijent(Pacijent pacijent) {
        this.pacijent = pacijent;
    }

    private Pacijent pacijent;


    public Lijecnik getLijecnik() {
        return lijecnik;
    }

    public void setLijecnik(Lijecnik lijecnik) {
        this.lijecnik = lijecnik;
    }

    public String getNalaz() {
        return nalaz;
    }

    public void setNalaz(String nalaz) {
        this.nalaz = nalaz;
    }
}
