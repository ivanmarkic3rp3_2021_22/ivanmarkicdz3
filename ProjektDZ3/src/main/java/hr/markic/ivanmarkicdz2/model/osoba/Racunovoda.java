package hr.markic.ivanmarkicdz2.model.osoba;

import hr.markic.ivanmarkicdz2.model.narudzba.Bilanca;

public class Racunovoda extends Zaposlenik{

    private Bilanca bilanca;

    public Bilanca getBilanca() {
        return bilanca;
    }

    public void setBilanca(Bilanca bilanca) {
        this.bilanca = bilanca;
    }

    public boolean urediPlace(){
        return false;
    };

    public boolean urediTroskove(){
        return false;
    };

    public boolean urediRacune(){
        return false;
    };
}
