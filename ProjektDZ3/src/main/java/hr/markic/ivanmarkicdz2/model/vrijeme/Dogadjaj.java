package hr.markic.ivanmarkicdz2.model.vrijeme;

import hr.markic.ivanmarkicdz2.model.osoba.Osoba;

import java.util.ArrayList;
import java.util.List;

public class Dogadjaj {

    private int tipDogadaja;
    private List<Termin> termini;
    private final List<Osoba> osobe;

    public Dogadjaj(List<Osoba> osobe) {
        this.osobe = osobe;
        termini = new ArrayList<>();
    }

    public List<Termin> getTermini() {
        return termini;
    }

    public void setTermini(List<Termin> termini) {
        this.termini = termini;
    }

    public List<Osoba> getOsobe() {
        return osobe;
    }

    public int getTipDogadaja() {
        return tipDogadaja;
    }

    public void setTipDogadaja(int tipDogadaja) {
        this.tipDogadaja = tipDogadaja;
    }
}
