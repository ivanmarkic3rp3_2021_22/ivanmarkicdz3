package hr.markic.ivanmarkicdz2.model.narudzba;

import hr.markic.ivanmarkicdz2.model.osoba.ZaposlenikSluzbeNabave;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Narudzba {

    private Date datum;
    private List<StavkaNarudzbe> stavkeNarudzbe;
    private final ZaposlenikSluzbeNabave zaposlenikSluzbeNabave;

    public List<StavkaNarudzbe> getStavkeNarudzbe() {
        if (stavkeNarudzbe == null){
            stavkeNarudzbe = new ArrayList<>();
        }
        return stavkeNarudzbe;
    }

    public void setStavkeNarudzbe(List<StavkaNarudzbe> stavkeNarudzbe) {
        this.stavkeNarudzbe = stavkeNarudzbe;
    }

    public Narudzba(ZaposlenikSluzbeNabave zaposlenikSluzbeNabave) {
        this.zaposlenikSluzbeNabave = zaposlenikSluzbeNabave;
        this.stavkeNarudzbe = new ArrayList<>();
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public ZaposlenikSluzbeNabave getZaposlenikSluzbeNabave() {
        return zaposlenikSluzbeNabave;
    }

    public boolean dodajStavku(StavkaNarudzbe stavkaNarudzbe){
        if(stavkaNarudzbe != null){
            stavkeNarudzbe.add(stavkaNarudzbe);
            return true;
        }
        return false;
    }

    public boolean obrisiStavku(StavkaNarudzbe stavkaNarudzbe){
        if(stavkaNarudzbe != null){
            stavkeNarudzbe.remove(stavkaNarudzbe);
            return true;
        }
        return false;
    }
}
