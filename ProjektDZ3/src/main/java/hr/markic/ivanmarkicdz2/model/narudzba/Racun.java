package hr.markic.ivanmarkicdz2.model.narudzba;

import hr.markic.ivanmarkicdz2.model.osoba.MedicinskaSestra;

import java.util.Date;

public class Racun {

    private int id;
    private Date timeStamp;
    private Double cijena;
    private String opis;
    private MedicinskaSestra medicinskaSestra;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Double getCijena() {
        return cijena;
    }

    public void setCijena(Double cijena) {
        this.cijena = cijena;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public MedicinskaSestra getMedicinskaSestra() {
        return medicinskaSestra;
    }

    public void setMedicinskaSestra(MedicinskaSestra medicinskaSestra) {
        this.medicinskaSestra = medicinskaSestra;
    }

    public boolean naplati() {
        return false;
    }
}
