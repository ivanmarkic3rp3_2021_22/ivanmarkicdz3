package hr.markic.ivanmarkicdz2.model.logistika;

import hr.markic.ivanmarkicdz2.model.narudzba.Inventar;

import java.util.ArrayList;

public class Soba {
    private int id;
    private String naziv;
    private Inventar inventar;

    public Soba() {
        this.inventar = new Inventar(new ArrayList<>());
    }

    public Inventar getInventar() {
        return inventar;
    }

    public void setInventar(Inventar inventar) {
        this.inventar = inventar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Odjel getOdjel() {
        return odjel;
    }

    public void setOdjel(Odjel odjel) {
        this.odjel = odjel;
    }

    private Odjel odjel;
}
