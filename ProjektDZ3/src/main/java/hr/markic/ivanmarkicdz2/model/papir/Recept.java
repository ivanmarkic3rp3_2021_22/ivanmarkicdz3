package hr.markic.ivanmarkicdz2.model.papir;

import hr.markic.ivanmarkicdz2.model.osoba.Lijecnik;
import hr.markic.ivanmarkicdz2.model.osoba.Pacijent;

public class Recept {

    private String recept;
    private Lijecnik lijecnik;
    private Pacijent pacijent;

    public Lijecnik getLijecnik() {
        return lijecnik;
    }

    public Pacijent getPacijent() {
        return pacijent;
    }

    public void setPacijent(Pacijent pacijent) {
        this.pacijent = pacijent;
    }

    public void setLijecnik(Lijecnik lijecnik) {
        this.lijecnik = lijecnik;
    }

    public String getRecept() {
        return recept;
    }

    public void setRecept(String recept) {
        this.recept = recept;
    }
}
