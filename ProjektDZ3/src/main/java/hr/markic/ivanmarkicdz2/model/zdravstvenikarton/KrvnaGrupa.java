package hr.markic.ivanmarkicdz2.model.zdravstvenikarton;

public enum KrvnaGrupa {



    NULA_MINUS("0-"),
    NULA_PLUS("0+"),
    A_MINUS("A-"),
    A_PLUS("A+"),
    B_MINUS("B-"),
    B_PLUS("B+"),
    AB_MINUS("AB-"),
    AB_PLUS("AB+");

    private final String ime;

    public String getIme() {
        return ime;
    }

    KrvnaGrupa(String ime) {
        this.ime = ime;
    }


}
