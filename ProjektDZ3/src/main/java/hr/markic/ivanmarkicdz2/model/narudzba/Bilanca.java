package hr.markic.ivanmarkicdz2.model.narudzba;

import hr.markic.ivanmarkicdz2.model.osoba.Racunovoda;

import java.util.List;

public class Bilanca {

    private Double saldo;
    private Racunovoda racunovoda;
    private final List<Racun> racun;
    private final List<Inventar> inventar;

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Racunovoda getRacunovoda() {
        return racunovoda;
    }

    public void setRacunovoda(Racunovoda racunovoda) {
        this.racunovoda = racunovoda;
    }

    public List<Racun> getRacun() {
        return racun;
    }

    public List<Inventar> getInventar() {
        return inventar;
    }



    public Bilanca(List<Racun> racun, List<Inventar> inventar) {
        this.racun = racun;
        this.inventar = inventar;
    }

    public boolean procesiraj() {
        return false;
    }
}
