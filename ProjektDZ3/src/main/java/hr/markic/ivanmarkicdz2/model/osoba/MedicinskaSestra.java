package hr.markic.ivanmarkicdz2.model.osoba;

import hr.markic.ivanmarkicdz2.model.narudzba.Racun;
import hr.markic.ivanmarkicdz2.model.papir.Uputnica;
import hr.markic.ivanmarkicdz2.model.zdravstvenikarton.ZdravstveniKarton;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MedicinskaSestra extends Zaposlenik{

    private Pacijent pacijent;
    private List<Uputnica> uputnice;
    private List<Racun> racuni;

    public List<Racun> getRacuni() {
        if (racuni == null){
            racuni = new ArrayList<>();
        }
        return racuni;
    }

    public void setRacuni(List<Racun> racuni) {
        this.racuni = racuni;
    }

    public List<Uputnica> getUputnice() {
        if (uputnice == null) {
            uputnice = new ArrayList<>();
        }
        return uputnice;
    }

    public void setUputnice(List<Uputnica> uputnice) {
        this.uputnice = uputnice;
    }

    public Pacijent getPacijent() {
        return pacijent;
    }

    public void setPacijent(Pacijent pacijent) {
        this.pacijent = pacijent;
    }

    public Boolean upaliAlarm() {
        return false;
    }

    public Boolean urediAdministrativnePodatke() {
        return false;
    }

    public Boolean slanjeIzvjesca() {
        return false;
    }

    public Racun izdajRacun() {
        Racun racun = new Racun();
        racun.setId(1);
        racun.setCijena(20.0);
        racun.setMedicinskaSestra(new MedicinskaSestra());
        racun.setTimeStamp(new Date());
        racun.setOpis("Opis");
        return racun;
    }

    public Uputnica izdajUputnicu() {
        Uputnica uputnica = new Uputnica();
        uputnica.setSifraSpecijalistickogPregleda("sifraSpecijalistickogPregleda");
        uputnica.setSifraBolnickogLijecenja("SifraBolnickogLijecenja");
        uputnica.setSifraDijagnostickePretrage("SifraDijagnostickePretrage");
        uputnica.setSifraDijagnostickePretrage("SifraDijagnostickePretrage");
        uputnica.setSifraAmulantnogLijecenja("SifraAmulantnogLijecenja");
        uputnica.setNazivZdravstveneUstanove("NazivZdravstveneUstanove");
        uputnica.setNapomenaOBolesti("NapomenaOBolesti");
        uputnica.setNapomenaOTerapiji("NapomenaOTerapiji");
        uputnica.setDatumIzdavanja(new Date());
        uputnica.setPacijent(new Pacijent(new ZdravstveniKarton(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>()));
        uputnica.setLijecnik(new Lijecnik());
        uputnica.setMedicinskaSestra(new MedicinskaSestra());
        return uputnica;
    }

    public Boolean slanjeZahtjevaNabavi() {
        return false;
    }

    public Boolean urediRaspored() {
        return false;
    }

}
