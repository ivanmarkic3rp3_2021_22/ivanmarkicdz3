package hr.markic.ivanmarkicdz2.model.osoba;

import hr.markic.ivanmarkicdz2.model.papir.Nalaz;
import hr.markic.ivanmarkicdz2.model.papir.Recept;
import hr.markic.ivanmarkicdz2.model.papir.Uputnica;
import hr.markic.ivanmarkicdz2.model.zdravstvenikarton.ZdravstveniKarton;

import java.util.List;

public class Pacijent extends Osoba {

    private String brojOsiguranja;
    private final ZdravstveniKarton zdravstveniKarton;
    private Lijecnik lijecnik;
    private final List<Uputnica> uputnice;
    private final List<Recept> recepti;
    private final List<Nalaz> nalazi;

    public Lijecnik getLijecnik() {
        return lijecnik;
    }

    public void setLijecnik(Lijecnik lijecnik) {
        this.lijecnik = lijecnik;
    }



    public List<Recept> getRecepti() {
        return recepti;
    }

    public List<Nalaz> getNalazi() {
        return nalazi;
    }

    public List<Uputnica> getUputnice() {
        return uputnice;
    }

    public String getBrojOsiguranja() {
        return brojOsiguranja;
    }

    public void setBrojOsiguranja(String brojOsiguranja) {
        this.brojOsiguranja = brojOsiguranja;
    }

    public ZdravstveniKarton getZdravstveniKarton() {
        ZdravstveniKarton copy = zdravstveniKarton;
        return  copy;
    }


    public Pacijent(ZdravstveniKarton zdravstveniKarton, List<Uputnica> uputnice, List<Recept> recepti, List<Nalaz> nalazi) {
        this.zdravstveniKarton = zdravstveniKarton;
        this.uputnice = uputnice;
        this.recepti = recepti;
        this.nalazi = nalazi;
    }

    public Boolean rezervirajTermin() {
        return false;
    }

    public void pogledajZdravstveniKarton() {

    }

    public Boolean promijeniLijecnika() {
        return false;
    }

}
