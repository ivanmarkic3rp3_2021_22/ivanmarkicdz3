package hr.markic.ivanmarkicdz2.model.zdravstvenikarton;

import hr.markic.ivanmarkicdz2.model.osoba.Pacijent;
import hr.markic.ivanmarkicdz2.model.osoba.Zaposlenik;

import java.util.Date;

public class Dokument {

    private int tip;
    private Date datumStvaranja;
    private Zaposlenik autor;
    private Pacijent pacijent;
    private String sadrzaj;

    public int getTip() {
        return tip;
    }

    public void setTip(int tip) {
        this.tip = tip;
    }

    public Date getDatumStvaranja() {
        return datumStvaranja;
    }

    public void setDatumStvaranja(Date datumStvaranja) {
        this.datumStvaranja = datumStvaranja;
    }

    public Zaposlenik getAutor() {
        return autor;
    }

    public void setAutor(Zaposlenik autor) {
        this.autor = autor;
    }

    public Pacijent getPacijent() {
        return pacijent;
    }

    public void setPacijent(Pacijent pacijent) {
        this.pacijent = pacijent;
    }

    public String getSadrzaj() {
        return sadrzaj;
    }

    public void setSadrzaj(String sadrzaj) {
        this.sadrzaj = sadrzaj;
    }
}
