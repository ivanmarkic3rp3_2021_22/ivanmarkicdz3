package hr.markic.ivanmarkicdz2.model.osoba;

import hr.markic.ivanmarkicdz2.model.vrijeme.Raspored;

import java.util.Date;

public abstract class Osoba {

    private String ime;
    private String prezime;
    private Date datumRodjenja;
    private String korisnickoIme;
    private String lozinka;
    private Raspored raspored = new Raspored();

    public Raspored getRaspored() {
        return raspored;
    }

    public void setRaspored(Raspored raspored) {
        this.raspored = raspored;
    }

    public boolean login() {
      return false;
    }

    public boolean logout() {
        return false;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public Date getDatumRodjenja() {
        return datumRodjenja;
    }

    public void setDatumRodjenja(Date datumRodjenja) {
        this.datumRodjenja = datumRodjenja;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }
}
