package hr.markic.ivanmarkicdz2.model.osoba;

import hr.markic.ivanmarkicdz2.model.papir.Nalaz;
import hr.markic.ivanmarkicdz2.model.papir.Recept;
import hr.markic.ivanmarkicdz2.model.papir.Uputnica;

import java.util.ArrayList;
import java.util.List;

public class Lijecnik extends Zaposlenik {

    private Pacijent pacijent;
    private List<Uputnica> uputnice;
    private Nalaz nalaz;
    private List<Recept> recepti;

    public Pacijent getPacijent() {
        return pacijent;
    }

    public void setPacijent(Pacijent pacijent) {
        this.pacijent = pacijent;
    }

    public Nalaz getNalaz() {
        return nalaz;
    }

    public void setNalaz(Nalaz nalaz) {
        this.nalaz = nalaz;
    }

    public List<Recept> getRecepti() {
        if (recepti == null) {
            recepti = new ArrayList<>();
        }
        return recepti;
    }

    public void setRecepti(List<Recept> recepti) {
        this.recepti = recepti;
    }

    public List<Uputnica> getUputnice() {

        if (uputnice == null) {
            uputnice = new ArrayList<>();
        }
        return uputnice;
    }

    public void setUputnice(List<Uputnica> uputnice) {
        this.uputnice = uputnice;
    }

    public Uputnica izdajUputnicu() {
        return new Uputnica();
    }

    public Recept izdajRecept() {
        return new Recept();
    }

    public Nalaz izdajNalaz() {
        return new Nalaz();
    }

    public boolean spremiIzvjesce() {
        return false;
    }
}
