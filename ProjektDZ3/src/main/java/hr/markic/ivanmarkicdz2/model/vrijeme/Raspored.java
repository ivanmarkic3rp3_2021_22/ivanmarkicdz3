package hr.markic.ivanmarkicdz2.model.vrijeme;

import java.util.ArrayList;
import java.util.List;

public class Raspored {


    private List<Termin> termini;

    public Raspored() {
        this.termini = new ArrayList<>();
    }

    public List<Termin> getTermini() {
        return termini;
    }

    public void setTermini(List<Termin> termini) {
        this.termini = termini;
    }

    public boolean dodajTermin(Termin termin) {
        if (termin != null){
            termini.add(termin);
            return true;
        }
        return false;
    }

    public boolean obrisiTermin(Termin termin) {
        if (termin != null){
            termini.remove(termin);
            return true;
        }
        return false;
    }

    public void prikaziRaspored() {

    }
}
