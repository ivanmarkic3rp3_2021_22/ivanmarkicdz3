package hr.markic.ivanmarkicdz2.model.papir;

import hr.markic.ivanmarkicdz2.model.osoba.Lijecnik;
import hr.markic.ivanmarkicdz2.model.osoba.MedicinskaSestra;
import hr.markic.ivanmarkicdz2.model.osoba.Pacijent;

import java.util.Date;

public class Uputnica {

    private String sifraSpecijalistickogPregleda;
    private String sifraBolnickogLijecenja;
    private String sifraDijagnostickePretrage;
    private String sifraAmulantnogLijecenja;
    private String nazivZdravstveneUstanove;
    private String napomenaOBolesti;
    private String napomenaOTerapiji;
    private Date datumIzdavanja;
    private Pacijent pacijent;
    private Lijecnik lijecnik;
    private MedicinskaSestra medicinskaSestra;

    public Pacijent getPacijent() {
        return pacijent;
    }

    public void setPacijent(Pacijent pacijent) {
        this.pacijent = pacijent;
    }



    public MedicinskaSestra getMedicinskaSestra() {
        return medicinskaSestra;
    }

    public void setMedicinskaSestra(MedicinskaSestra medicinskaSestra) {
        this.medicinskaSestra = medicinskaSestra;
    }

    public Lijecnik getLijecnik() {
        return lijecnik;
    }

    public void setLijecnik(Lijecnik lijecnik) {
        this.lijecnik = lijecnik;
    }

    public String getSifraSpecijalistickogPregleda() {
        return sifraSpecijalistickogPregleda;
    }

    public void setSifraSpecijalistickogPregleda(String sifraSpecijalistickogPregleda) {
        this.sifraSpecijalistickogPregleda = sifraSpecijalistickogPregleda;
    }

    public String getSifraBolnickogLijecenja() {
        return sifraBolnickogLijecenja;
    }

    public void setSifraBolnickogLijecenja(String sifraBolnickogLijecenja) {
        this.sifraBolnickogLijecenja = sifraBolnickogLijecenja;
    }

    public String getSifraDijagnostickePretrage() {
        return sifraDijagnostickePretrage;
    }

    public void setSifraDijagnostickePretrage(String sifraDijagnostickePretrage) {
        this.sifraDijagnostickePretrage = sifraDijagnostickePretrage;
    }

    public String getSifraAmulantnogLijecenja() {
        return sifraAmulantnogLijecenja;
    }

    public void setSifraAmulantnogLijecenja(String sifraAmulantnogLijecenja) {
        this.sifraAmulantnogLijecenja = sifraAmulantnogLijecenja;
    }

    public String getNazivZdravstveneUstanove() {
        return nazivZdravstveneUstanove;
    }

    public void setNazivZdravstveneUstanove(String nazivZdravstveneUstanove) {
        this.nazivZdravstveneUstanove = nazivZdravstveneUstanove;
    }

    public String getNapomenaOBolesti() {
        return napomenaOBolesti;
    }

    public void setNapomenaOBolesti(String napomenaOBolesti) {
        this.napomenaOBolesti = napomenaOBolesti;
    }

    public String getNapomenaOTerapiji() {
        return napomenaOTerapiji;
    }

    public void setNapomenaOTerapiji(String napomenaOTerapiji) {
        this.napomenaOTerapiji = napomenaOTerapiji;
    }

    public Date getDatumIzdavanja() {
        return datumIzdavanja;
    }

    public void setDatumIzdavanja(Date datumIzdavanja) {
        this.datumIzdavanja = datumIzdavanja;
    }

}
