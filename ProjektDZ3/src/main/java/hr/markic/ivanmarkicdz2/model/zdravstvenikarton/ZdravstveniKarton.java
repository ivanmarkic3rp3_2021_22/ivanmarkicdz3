package hr.markic.ivanmarkicdz2.model.zdravstvenikarton;

import java.util.ArrayList;
import java.util.List;

public class ZdravstveniKarton {

    private KrvnaGrupa krvnaGrupa;
    private List<Dokument> dokumenti;

    public ZdravstveniKarton() {
        this.krvnaGrupa = KrvnaGrupa.A_MINUS;
        this.dokumenti = new ArrayList<>();
    }

    public KrvnaGrupa getKrvnaGrupa() {
        return krvnaGrupa;
    }

    public void setKrvnaGrupa(KrvnaGrupa krvnaGrupa) {
        this.krvnaGrupa = krvnaGrupa;
    }

    public List<Dokument> getDokumenti() {
        return dokumenti;
    }

    public void setDokumenti(List<Dokument> dokumenti) {
        this.dokumenti = dokumenti;
    }

    public boolean dodajDokument() {
        return false;
    }

    public boolean brisiDokument() {
        return false;
    }


}
