package hr.markic.ivanmarkicdz2.model.osoba;

import java.util.Date;

public class Zaposlenik extends Osoba {

    private Date datumZaposljavanja;

    public Date getDatumZaposljavanja() {
        return datumZaposljavanja;
    }

    public void setDatumZaposljavanja(Date datumZaposljavanja) {
        this.datumZaposljavanja = datumZaposljavanja;
    }


}
