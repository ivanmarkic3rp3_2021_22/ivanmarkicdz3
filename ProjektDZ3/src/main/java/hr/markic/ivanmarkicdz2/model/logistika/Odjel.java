package hr.markic.ivanmarkicdz2.model.logistika;

import hr.markic.ivanmarkicdz2.model.osoba.Zaposlenik;

import java.util.ArrayList;
import java.util.List;

public class Odjel {

    private String nazivOdjela;
    private List<Zaposlenik> zaposlenici;
    private List<Soba> sobe;

    public List<Soba> getSobe() {
        return sobe;
    }

    public void setSobe(List<Soba> sobe) {
        this.sobe = sobe;
    }

    public void setZaposlenici(List<Zaposlenik> zaposlenici) {
        this.zaposlenici = zaposlenici;
    }

    public Odjel(List<Zaposlenik> zaposlenici) {
        this.zaposlenici = zaposlenici;
        this.sobe = new ArrayList<>();
    }

    public String getNazivOdjela() {
        return nazivOdjela;
    }

    public void setNazivOdjela(String nazivOdjela) {
        this.nazivOdjela = nazivOdjela;
    }

    public List<Zaposlenik> getZaposlenici() {
        return zaposlenici;
    }
}
