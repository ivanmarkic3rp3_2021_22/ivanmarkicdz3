package hr.markic.ivanmarkicdz2.model.narudzba;

import java.util.List;

public class Inventar {

    private final List<Predmet> predmeti;

    public Inventar(List<Predmet> predmeti) {
        this.predmeti = predmeti;
    }

    public List<Predmet> getPredmeti() {
        return predmeti;
    }

    public boolean dodaj() {
        return false;
    }
}
